# Supported tags and respective `Dockerfile` links
- 18.3.2-r0 [(Dockerfile)](https://bitbucket.org/henhoteam/docker-erlang/src/da754bc363e75827d209d91138cc236a9ea1b85b/Dockerfile?at=18.3.2-r0&fileviewer=file-view-default)

This erlang image is intended to be used as base image for other image.
The tag version is version of erlang OTP.
